const gulp = require('gulp');
const zip = require('gulp-zip');
const fileindex = require('gulp-fileindex');

gulp.task('fileindex', function() {
    return gulp.src('dist/*.zip')
        .pipe(fileindex())
        .pipe(gulp.dest('./dist'));
});

gulp.task('zipper1', function() {
    return gulp.src('src/ServicioCliente/web/*')
        .pipe(zip('ServicioCliente.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('zipper2', function() {
    return gulp.src('src/ESB/web/*')
        .pipe(zip('ESB.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('zipper3', function() {
    return gulp.src('src/ServicioPiloto/web/*')
        .pipe(zip('ServicioPiloto.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('zipper4', function() {
    return gulp.src('src/ServicioUbicacion/web/*')
        .pipe(zip('ServicioUbicacion.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.series('zipper1', 'zipper2', 'zipper3', 'zipper4', 'fileindex'));
