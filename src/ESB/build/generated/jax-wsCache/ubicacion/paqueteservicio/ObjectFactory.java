
package paqueteservicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the paqueteservicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Ubicacion_QNAME = new QName("http://paqueteServicio/", "ubicacion");
    private final static QName _UbicacionResponse_QNAME = new QName("http://paqueteServicio/", "ubicacionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: paqueteservicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Ubicacion_Type }
     * 
     */
    public Ubicacion_Type createUbicacion_Type() {
        return new Ubicacion_Type();
    }

    /**
     * Create an instance of {@link UbicacionResponse }
     * 
     */
    public UbicacionResponse createUbicacionResponse() {
        return new UbicacionResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ubicacion_Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "ubicacion")
    public JAXBElement<Ubicacion_Type> createUbicacion(Ubicacion_Type value) {
        return new JAXBElement<Ubicacion_Type>(_Ubicacion_QNAME, Ubicacion_Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "ubicacionResponse")
    public JAXBElement<UbicacionResponse> createUbicacionResponse(UbicacionResponse value) {
        return new JAXBElement<UbicacionResponse>(_UbicacionResponse_QNAME, UbicacionResponse.class, null, value);
    }

}
