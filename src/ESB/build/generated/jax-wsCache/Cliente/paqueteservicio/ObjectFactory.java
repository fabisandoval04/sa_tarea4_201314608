
package paqueteservicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the paqueteservicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cliente_QNAME = new QName("http://paqueteServicio/", "cliente");
    private final static QName _ClienteResponse_QNAME = new QName("http://paqueteServicio/", "clienteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: paqueteservicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cliente_Type }
     * 
     */
    public Cliente_Type createCliente_Type() {
        return new Cliente_Type();
    }

    /**
     * Create an instance of {@link ClienteResponse }
     * 
     */
    public ClienteResponse createClienteResponse() {
        return new ClienteResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cliente_Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "cliente")
    public JAXBElement<Cliente_Type> createCliente(Cliente_Type value) {
        return new JAXBElement<Cliente_Type>(_Cliente_QNAME, Cliente_Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "clienteResponse")
    public JAXBElement<ClienteResponse> createClienteResponse(ClienteResponse value) {
        return new JAXBElement<ClienteResponse>(_ClienteResponse_QNAME, ClienteResponse.class, null, value);
    }

}
