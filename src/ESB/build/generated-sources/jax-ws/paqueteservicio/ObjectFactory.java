
package paqueteservicio;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the paqueteservicio package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Piloto_QNAME = new QName("http://paqueteServicio/", "piloto");
    private final static QName _PilotoResponse_QNAME = new QName("http://paqueteServicio/", "pilotoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: paqueteservicio
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Piloto_Type }
     * 
     */
    public Piloto_Type createPiloto_Type() {
        return new Piloto_Type();
    }

    /**
     * Create an instance of {@link PilotoResponse }
     * 
     */
    public PilotoResponse createPilotoResponse() {
        return new PilotoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Piloto_Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "piloto")
    public JAXBElement<Piloto_Type> createPiloto(Piloto_Type value) {
        return new JAXBElement<Piloto_Type>(_Piloto_QNAME, Piloto_Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PilotoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://paqueteServicio/", name = "pilotoResponse")
    public JAXBElement<PilotoResponse> createPilotoResponse(PilotoResponse value) {
        return new JAXBElement<PilotoResponse>(_PilotoResponse_QNAME, PilotoResponse.class, null, value);
    }

}
