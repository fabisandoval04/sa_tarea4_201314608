/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteServicio;

import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Fabi
 */
@WebService(serviceName = "ubicacion")
public class ubicacion {

    public static  ArrayList<Piloto> pilotos=new ArrayList<Piloto>();
    /**
     * Servicio ubicacion
     */
    @WebMethod(operationName = "ubicacion")
    public String ubicacion(@WebParam(name = "ubicacion") String ubicacion) {
         Piloto p=new Piloto("fabi","fabi");
        
        /*Seleccion del piloto mas cercano */
        for(int i=0; i<pilotos.size();i++){
            if(pilotos.get(i).ubicacion.toUpperCase().equals(ubicacion.toUpperCase())){
                p=pilotos.get(i);
            }
        }
        System.out.println("Ubicacion "+p.nombre); 
        return p.nombre;
    }
}
